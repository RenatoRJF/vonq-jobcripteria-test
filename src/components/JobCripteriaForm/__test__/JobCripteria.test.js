import React from 'react';
import { shallow } from 'enzyme';

import { findByTestAttr } from '../../../../test/testUtils';

import store from '../../../store';

import { JobCripteriaForm } from '../JobCripteriaForm';

const componentWrapper = (props={}) => {
  return shallow(<JobCripteriaForm {...props} store={store}/>);
}

let wrapper;

beforeEach(() => {
  wrapper = componentWrapper();
});

describe('When form starts', () => {
  test('renders without error', () => {
    const component = findByTestAttr(wrapper, 'component-form');
    expect(component.exists()).toBe(true);
  });

  test('renders an `input` to type years of experience', () => {
    const component = findByTestAttr(wrapper, 'experience');
    expect(component.exists()).toBe(true);
  });

  test('renders an `checkbox group` to select an education level', () => {
    const component = findByTestAttr(wrapper, 'education-level');
    expect(component.exists()).toBe(true);
  });

  test('renders an `input` to type the min hours of work per week', () => {
    const component = findByTestAttr(wrapper, 'min-hours');
    expect(component.exists()).toBe(true);
  });

  test('renders an `input` to type the max hours of work per week', () => {
    const component = findByTestAttr(wrapper, 'max-hours');
    expect(component.exists()).toBe(true);
  });

  test('renders `save` button', () => {
    const component = findByTestAttr(wrapper, 'save-button');
    expect(component.exists()).toBe(true);
  });
});


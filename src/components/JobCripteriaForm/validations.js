// Required
export const required = value => {
  const message = 'The field is required';

  if (Array.isArray(value)) {
    return value.length > 0 ? undefined : message;
  }

  return value ? undefined : message;
}

// Min value
export const rangeHours = val => {
  const min = 30;
  const max = 40;
  const isValid = val && (val >= min) && (val <= max);

  return isValid ? undefined : `This field has a range from ${min} to ${max}`;
}

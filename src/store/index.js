import { createStore, combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form'

import jobCripteria from './reducers/jobCripteria';

const reducer = combineReducers({
  form: reduxFormReducer,
  jobCripteria,
})


const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;

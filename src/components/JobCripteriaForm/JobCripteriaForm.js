import React, { Component } from 'react';
import { Field } from 'redux-form';

import './styles.scss';

import FormInput from '../forms/FormInput';
import Button from '../forms/Button';
import FormCheckboxGroup from '../forms/FormCheckboxGroup';

import { required, rangeHours } from './validations'

const InputNumber = ({ name, label, validate }) => (
  <Field
    name={name}
    type="text"
    component={FormInput}
    label={label}
    className="number-input"
    validate={validate}
    parse={val => val ? (parseInt(val, 10) || null) : null}
  />
);

const options = [
  'Bachelor / Graduate',
  'GCSE / A-Level / Highschool / GED',
  'Master / Post-Graduate / PhD',
  'Vocational / Diploma / Associates degree',
]

export class JobCripteriaForm extends Component {
  render() {
    const { handleSubmit, pristine, reset, submitting, editing, cancel } = this.props;

    return (
      <div data-test="component-form" className="form-wrapper">
        <form onSubmit={handleSubmit}>

          <h3 className="title">
            <span className="title__number">1</span>
            Deteails settings
          </h3>

          <p className="description">
            We will use the data we collect here to better target your desired audience.
          </p>

          <label htmlFor="yearsExp">A minimum No. years of experience</label>

          <InputNumber
            data-test="experience"
            name="yearsExp"
            label="e.g. 5+"
            validate={[required]} />

          <div className="education-level">
            <label htmlFor="yearsExp">Level of education</label>
            <Field
              data-test="education-level"
              name="educationLevel"
              component={FormCheckboxGroup}
              options={options}
              validate={[required]}
            />
          </div>

          <label htmlFor="yearsExp">A minimum No. years of experience</label>

          <div className="form-group">
            <div className="input-wrapper">
              <label htmlFor="min">Min.</label>
              <InputNumber
                data-test="min-hours"
                name="minWeekHours"
                label="e.g. 30+"
                validate={[required, rangeHours]} />
            </div>

            <div className="input-wrapper">
              <label htmlFor="min">Max.</label>
              <InputNumber
                data-test="max-hours"
                name="maxWeekHours"
                label="e.g. 40-"
                validate={[required, rangeHours]} />
            </div>
          </div>

          <div className="button-wrapper">
            {
              editing &&
              <Button
                label="Cancel"
                className="btn light"
                type="button"
                onClick={cancel}
              />
            }

            <Button
              label="Reset"
              className="btn primary"
              type="button"
              disabled={pristine || submitting}
              onClick={reset}
            />

            <Button
              data-test="save-button"
              label={`${editing ? 'Save' : 'Save and Continue'}`}
              className="btn success"
              type="submit"
              disabled={submitting || (editing && pristine)}
              loading={submitting}
            />
          </div>
        </form>
      </div>
    );
  }
}

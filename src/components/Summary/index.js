import React from 'react';

import Button from '../forms/Button';

import IconDone from '../../assets/icons/icon-done.svg';

import './styles.scss';

const Summary = ({ data, onEdit, ...rest }) => {
  const { yearsExp, minWeekHours, maxWeekHours, educationLevel } = data;

  return (
    <div className="summary">
      <header className="summary__header">
        <h3 className="title">
          <img src={IconDone} alt="icon done" className="title__icon" />
          Summary
				</h3>

        <Button className="primary" label="Edit" onClick={onEdit} />
      </header>

      <div className="summary__content">
        <ul className="details">
          <li>
            A minimum No. years of experience:
            <strong>{yearsExp}</strong>
          </li>

          <li>
            No. of working hours (per week):
            <strong>{`${minWeekHours} - ${maxWeekHours} hours`}</strong>
          </li>

          <li>
            <h4 className="details__title">Level of education</h4>

            <div className="education-levels">
              {educationLevel.map(edLevel => <span key={edLevel}>{edLevel}</span>)}
            </div>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default Summary;

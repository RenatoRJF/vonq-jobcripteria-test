import React from 'react';

import './styles.scss';

import IconWarning from '../../../assets/icons/icon-warning.svg';

const ErrorMessage = props => (
  <div className="validation-message">
    <img src={IconWarning} alt="warning" />
    <span>{props.message}</span>
  </div>
);

export default ErrorMessage;

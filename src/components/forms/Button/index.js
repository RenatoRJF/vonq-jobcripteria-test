import React from 'react';

import './styles.scss';

import Loader from '../../Loader';

const Button = props => (
  <button
    type={props.type}
    className={props.className}
    disabled={props.disabled}
    style={props.loading ? { color: 'transparent'} : null}
    onClick={props.onClick}>

    {props.label}

    {props.loading && <Loader loading={props.loading} />}
  </button>
);

export default Button;

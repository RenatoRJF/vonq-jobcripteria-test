import React, { Component } from 'react';
import { connect } from 'react-redux';

import './styles.scss';

import { loadData } from '../../store/actions/jobCripteria';

import JobCripteriaForm from '../JobCripteriaForm';
import Summary from '../Summary';

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showSummary: false,
    }
  }

  handleEdit = () => {
    this.setState({ showSummary: false });
  }

  handleSubmit = (values, dispatch) => {
    // Simulate server latency
    const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

    return sleep(1000).then(() => {
      dispatch(loadData(values))
      this.showSummaryContent();
    });
  }

  showSummaryContent = () => {
    this.setState({ showSummary: true });
  }

  render() {
    const { showSummary } = this.state;
    const { data } = this.props;
    const editing = data !== undefined;

    return (
      <div className="main">
        {
          showSummary
            ? <Summary data={data} onEdit={this.handleEdit} />
            : (
              <JobCripteriaForm
                editing={editing}
                onSubmit={this.handleSubmit}
                cancel={this.showSummaryContent}
              />
            )
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  data: state.jobCripteria.data,
});

export default connect(mapStateToProps)(Main);

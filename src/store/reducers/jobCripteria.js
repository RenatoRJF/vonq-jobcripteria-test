import { LOAD } from '../actions/jobCripteria';

const jobCripteria = (state = {}, action) => {
  switch (action.type) {
    case LOAD:
      return { data: action.data }
    default:
      return state;
  }
}

export default jobCripteria;

import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

import { JobCripteriaForm as Form } from './JobCripteriaForm';

// Initial values and
const InitialData = {
  minWeekHours: 30,
  maxWeekHours: 40,
  educationLevel: []
};

const JobCripteriaForm = reduxForm({
  form: 'submitValidation',
  nableReinitialize: true,
  touchOnBlur: false
})(Form);

export default connect(
  state => ({
    initialValues: state.jobCripteria.data || InitialData
  }),
)(JobCripteriaForm);

import React from 'react';

import './styles.scss';

import ErrorMessage from '../ErrorMessage';

const FormInput = ({
  input,
  meta: { touched, error },
  className,
  pattern,
  label,
  ...rest
}) => {
  const inputClass = (touched && error) ? `${className} invalid-field` : className;

  return (
    <div>
      <input
        {...input}
        {...rest}
        pattern={pattern}
        className={inputClass}
        placeholder={label}
      />

      {touched && error && <ErrorMessage message={error} />}
    </div>
  )
};

export default FormInput;

import React from 'react';
import { Checkbox, CheckboxGroup } from 'react-checkbox-group';

import './styles.scss';

import ErrorMessage from '../ErrorMessage';

const FormCheckboxGroup = ({
  input: { value, onChange },
  meta: { touched, error },
  options,
  className,
  ...rest
}) => {
  const errorClass = (touched && error) ? 'invalid-field' : '';

  return (
    <div>
      <div className={`input-checkbox-group ${className} ${errorClass}`}>

        <CheckboxGroup {...rest} value={value} onChange={onChange} checkboxDepth={3}>
          {options.map(option =>
            <div key={option}>
              <label>
                <Checkbox value={option} /> {option}
              </label>
            </div>
          )}
        </CheckboxGroup>

      </div>

      {touched && error && <ErrorMessage message={error} />}
    </div>
  )
};

export default FormCheckboxGroup;

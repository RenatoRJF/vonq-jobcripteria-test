import React from 'react';

import './styles.scss';

const Loader = props => props.loading ? <div className="loader"></div> : null;

export default Loader;

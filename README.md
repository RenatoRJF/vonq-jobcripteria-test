# Job Cripteria Test

[Click here to see DEMO](https://festive-goldberg-7e163f.netlify.com/)

This project consists of two main components that represent criteria for a job: A form for configuration and a summary to show the saved content.


### Project configuration

For the creation of the project I chose to start with `create-react-app` to
make project startup simpler and ensure a bit more stability in dependency compatibility.

### **JobCripteriaFrom component**

To create this component I decided to use `redux-form` to take advantage of its integration with redux, form states, events, etc. This also helped to add functionality to enable and disable form buttons with ease when saving and editing the data and manipulate the validations of the required fields.

### **Summary component**

 A simple component that is connected to the redux to update its contents whenever the form data is saved.
Running the project

### Auxiliary components
- #### Forms
  - FormInput
  - FormCheckboxGroup
  - Button
  - ErrorMessage

- #### Loader

### Requirements
- Closest mockups design when possible
- Error message for all required fields
- Default values ​​for weekly working hours
- Work in all modern browsers and IE11.
- Write tests

### Additional Functionalities
- Reset Button
- Cancel Button
- Loader

### Running the project

In the project directory, you can run:

**``npm start``** or **``yarn start``**

### Running tests

**``npm test``** or **``yarn test``**
